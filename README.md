Design patterns:

1. builder - třídy HouseBuilder, RoomBuilder
2. factory - třída DeviceFactory
3. singleton - třída House
4. observer - interface Subject (observable), třídy v package events (observers)
5. state machine - třídy v package electronicDevice.state a human.state
6. facade - třída Controller
7. iterator - třídy v package utils.iterators

Funkční požadavky:<br>
F1 máme - entity jsou rozmístěné do přísl ušných package<br>
F2 máme - entity v package electronic_device.api<br>
F3 máme - spotřeba je nastavena pro každý typ zařízení v příslušném konstruktoru<br>
F4 máme - v package trackers<br>
F5 mame - například ve tříde Father - metoda turnOffGasBoiler()<br>
F6 mame - ve tříde Controller<br>
F7 mame - řešíme v senzorech v package electronic_device a events<br>
F8 mame - v package reports<br>
F9 mame - ve třídě human metoda fixDevice()<br>
F10 mame - ve třídě controller při každém updatu bude každý člověk s 50 % pravděpodobností v příštím cyklu sportovat<br><br>
Nefunkční požadavky:<br>
Konfigurace domu je nahravana z jsonu (ve třídě utils.ConfigCreator lze vygenerovat konfigurační soubor).<br>
Vygenerované reporty i s příslušnými konfiguračními soubory jsou ve složce Reports_examples<br>
Javadoc je primárně ve třídě ElectronicDeviceApi<br>
Testy ke třídám ElectronicDeviceAPI a ActivityTracker v příslušných package<br>
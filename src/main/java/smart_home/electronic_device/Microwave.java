package smart_home.electronic_device;

import smart_home.utils.DeviceId;

public class Microwave extends ElectronicDevice{

    public Microwave() {
        super(7, 1, DeviceId.MICROWAVE);
    }


}

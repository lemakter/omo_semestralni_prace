package smart_home.electronic_device;

import lombok.Getter;
import smart_home.electronic_device.state.Active;
import smart_home.events.FridgeOutOfFoodEvent;
import smart_home.utils.DeviceId;

public class Fridge extends ElectronicDevice {

    @Getter
    private FridgeOutOfFoodEvent outOfFoodEvent;

    public Fridge() {
        super(4, 3, DeviceId.FRIDGE);
        this.status = new Active(this);
    }

    public void attach(FridgeOutOfFoodEvent event) {
        super.attach(event);
        this.outOfFoodEvent = event;
    }

    @Override
    public void fix() {
        status = new Active(this);
    }

}

package smart_home.electronic_device;

import lombok.Getter;
import smart_home.electronic_device.state.Active;
import smart_home.events.WashingMachineFullEvent;
import smart_home.utils.DeviceId;

public class WashingMachine extends ElectronicDevice {

    private static final int RUN_LEN = 4;

    @Getter
    private WashingMachineFullEvent fullEvent;

    public WashingMachine() {
        super(7, 2, DeviceId.WASHING_MACHINE);
    }

    public void attach(WashingMachineFullEvent event) {
        super.attach(event);
        fullEvent = event;
    }

    public void useDevice() {
        if (status.use()) {
            status = new Active(this);
            timeActive = RUN_LEN;
        }
    }
}

package smart_home.electronic_device;

import smart_home.utils.DeviceId;

public class Tv extends ElectronicDevice{

    public Tv() {
        super(4, 2, DeviceId.TV);
    }

}

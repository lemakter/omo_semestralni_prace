package smart_home.electronic_device;

import smart_home.utils.DeviceId;

public class Dryer extends ElectronicDevice {

    public static final int RUN_LEN = 4;

    public Dryer() {
        super(8, 1, DeviceId.DRYER);
    }

}

package smart_home.electronic_device.api;

import smart_home.House;
import smart_home.electronic_device.WashingMachine;
import smart_home.utils.DeviceId;

public class WashingMachineAPI extends ElectronicDeviceAPI {
    public WashingMachineAPI(WashingMachine device) {
        super(device);
    }

    @Override
    public void update() {
        super.update();
        if (device.getTimeActive() == 0) {
            for (ElectronicDeviceAPI device : House.getInstance().getAllDevices()) {
                if (device.getId().equals(DeviceId.DRYER)) {
                    ((DryerAPI) device).useDevice();
                    break;
                }
            }
        }
    }

    public void setFull() {
        ((WashingMachine) device).getFullEvent().update();
    }

}

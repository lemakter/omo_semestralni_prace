package smart_home.electronic_device.api;

import smart_home.electronic_device.GasBoiler;

public class GasBoilerAPI extends ElectronicDeviceAPI {

    public GasBoilerAPI(GasBoiler device) {
        super(device);
    }

    public void turnOff() {
        deviceBreakDown();
    }

    public void triggerGasLeak() {
        ((GasBoiler) device).getLeakEvent().update();
    }

}

package smart_home.electronic_device.api;

import smart_home.electronic_device.Dryer;
import smart_home.electronic_device.state.Active;

public class DryerAPI extends ElectronicDeviceAPI {

    public DryerAPI(Dryer device) {
        super(device);
    }

    public void useDevice() {
        if (device.getStatus().use()) {
            device.setStatus(new Active(device));
            device.setTimeActive(Dryer.RUN_LEN);
        }
    }
}

package smart_home.electronic_device.api;


import smart_home.electronic_device.Fridge;

public class FridgeAPI extends ElectronicDeviceAPI {
    public FridgeAPI(Fridge device) {
        super(device);
    }

    public void setNoFood() {
        ((Fridge) device).getOutOfFoodEvent().update();
    }
}

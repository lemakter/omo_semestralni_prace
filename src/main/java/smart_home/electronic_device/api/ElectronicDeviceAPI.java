package smart_home.electronic_device.api;

import lombok.AllArgsConstructor;
import smart_home.electronic_device.ElectronicDevice;
import smart_home.electronic_device.state.Active;
import smart_home.electronic_device.state.Idle;
import smart_home.human.Human;
import smart_home.utils.DeviceId;

@AllArgsConstructor
public class ElectronicDeviceAPI {

    protected final ElectronicDevice device;

    /**
     * Set device status to idle if device stopped working.
     * Decrement time active.
     * Update current device status.
     */
    public void update() {
        if (device.getTimeActive() == 0) {
            device.setStatus(new Idle(device));
        }
        device.setTimeActive(device.getTimeActive() - 1);
        device.getStatus().update();
    }

    /**
     * Set human to use the device for 1 unit of time.
     * @param human human who will use the device
     */
    public void useDevice(Human human) {
        useDevice(1, human);
    }

    /**
     * Set human to use the device for variable amount of time.
     * @param time amount of time the device will be used
     * @param human human who will use the device
     */
    public void useDevice(int time, Human human) {
        if (device.getStatus().use()) {
            device.setStatus(new Active(device));
            device.setTimeActive(time);
            if (human != null)
                human.setUsing(device);
        }
    }

    /**
     * Call deviceBreakDown() method which will set device status to Broken.
     */
    public void deviceBreakDown() {
        device.deviceBreakDown();
    }

    /**
     * Getter for device id attribute.
     * @return device id
     */
    public DeviceId getId() {
        return device.getId();
    }

    /**
     * Call fix() method which will set device status to Idle.
     */
    public void fix() {
        device.fix();
    }

    /**
     * ToString() method for device.
     * @return string notation of device
     */
    @Override
    public String toString() {
        return device.toString();
    }
}

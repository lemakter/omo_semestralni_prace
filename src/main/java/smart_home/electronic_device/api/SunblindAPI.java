package smart_home.electronic_device.api;

import smart_home.electronic_device.Sunblind;

public class SunblindAPI extends ElectronicDeviceAPI {
    public SunblindAPI(Sunblind device) {
        super(device);
    }


    public void lowerBlind() {
        if (((Sunblind) device).isUp()) {
            ((Sunblind) device).setUp(false);
            useDevice(1, null);
        }
    }

    public void riseBlind() {
        if (!((Sunblind) device).isUp()) {
            ((Sunblind) device).setUp(true);
            useDevice(1, null);
        }
    }

    @Override
    public void update() {
        super.update();
        lowerBlind();
    }
}

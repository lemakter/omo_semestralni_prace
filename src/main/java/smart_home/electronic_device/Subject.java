package smart_home.electronic_device;

import smart_home.events.EventObserver;

public interface Subject {

    void attach(EventObserver observer);

    void detach(EventObserver observer);

    void notifyObserver();
}

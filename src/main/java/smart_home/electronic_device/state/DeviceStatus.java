package smart_home.electronic_device.state;

import smart_home.electronic_device.ElectronicDevice;

public abstract class DeviceStatus {

    protected final ElectronicDevice device;

    protected DeviceStatus(ElectronicDevice device) {
        this.device = device;
    }

    public abstract void update();
    public abstract boolean use();
}

package smart_home.electronic_device.state;

import smart_home.electronic_device.ElectronicDevice;

public class Broken extends DeviceStatus {
    public Broken(ElectronicDevice device) {
        super(device);
    }

    @Override
    public void update() {
        device.getTracker().trackOffUsage();
        device.notifyToFix();
    }

    @Override
    public boolean use() {
        return false;
    }
}

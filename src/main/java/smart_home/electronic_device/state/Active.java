package smart_home.electronic_device.state;

import smart_home.electronic_device.ElectronicDevice;
import smart_home.utils.DeviceId;

public class Active extends DeviceStatus {

    public Active(ElectronicDevice device) {
        super(device);
    }

    @Override
    public void update() {
        device.getTracker().trackActiveUsage();
        if (device.getId().equals(DeviceId.FRIDGE) || device.getId().equals(DeviceId.GAS_BOILER)) {
            device.setActive();
        }
    }

    @Override
    public boolean use() {
        return false;
    }
}

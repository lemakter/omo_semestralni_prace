package smart_home.electronic_device.state;

import smart_home.electronic_device.ElectronicDevice;

public class Idle extends DeviceStatus {
    public Idle(ElectronicDevice device) {
        super(device);
    }

    @Override
    public void update() {
        device.getTracker().trackIdleUsage();
    }

    @Override
    public boolean use(){
        return true;
    }
}

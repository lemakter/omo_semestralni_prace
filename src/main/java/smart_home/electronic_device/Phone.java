package smart_home.electronic_device;

import smart_home.utils.DeviceId;

public class Phone extends ElectronicDevice{

    public Phone() {
        super(3, 1, DeviceId.PHONE);
    }

}

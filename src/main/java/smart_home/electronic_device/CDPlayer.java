package smart_home.electronic_device;

import smart_home.utils.DeviceId;

public class CDPlayer extends ElectronicDevice{

    public CDPlayer() {
        super(3, 1, DeviceId.CD_PLAYER);
    }

}

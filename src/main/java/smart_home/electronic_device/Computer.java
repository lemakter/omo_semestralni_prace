package smart_home.electronic_device;

import smart_home.utils.DeviceId;

public class Computer extends ElectronicDevice {

    public Computer() {
        super(5, 2, DeviceId.COMPUTER);
    }

}

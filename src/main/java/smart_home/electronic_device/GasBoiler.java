package smart_home.electronic_device;

import lombok.Getter;
import smart_home.electronic_device.state.Active;
import smart_home.events.GasLeakEvent;
import smart_home.utils.DeviceId;

public class GasBoiler extends ElectronicDevice {

    @Getter
    private GasLeakEvent leakEvent;

    public GasBoiler() {
        super(10, 3, DeviceId.GAS_BOILER);
        this.status = new Active(this);
    }

    public void attach(GasLeakEvent event) {
        super.attach(event);
        this.leakEvent = event;
    }

    @Override
    public void fix() {
        status = new Active(this);
    }
}

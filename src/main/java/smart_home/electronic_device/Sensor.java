package smart_home.electronic_device;

import lombok.NoArgsConstructor;
import smart_home.events.EventObserver;

import java.util.ArrayList;
import java.util.List;
@NoArgsConstructor
public class Sensor implements Subject {

    private final List<EventObserver> observers = new ArrayList<>();

    @Override
    public void attach(EventObserver observer) {
        observers.add(observer);
    }

    @Override
    public void detach(EventObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for(EventObserver observer: observers){
            observer.update();
        }
    }
}
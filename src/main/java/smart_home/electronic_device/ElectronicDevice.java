package smart_home.electronic_device;

import lombok.Getter;
import lombok.Setter;
import smart_home.House;
import smart_home.electronic_device.state.Active;
import smart_home.electronic_device.state.Broken;
import smart_home.electronic_device.state.DeviceStatus;
import smart_home.electronic_device.state.Idle;
import smart_home.events.DeviceBrokenEvent;
import smart_home.events.EventObserver;
import smart_home.human.Human;
import smart_home.trackers.DeviceTracker;
import smart_home.utils.DeviceId;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Getter
public abstract class ElectronicDevice extends Sensor {

    private static final Random r = new Random();

    private final List<EventObserver> observers = new ArrayList<>();
    DeviceBrokenEvent brokenEvent;

    private final House house = House.getInstance();

    protected int activeConsumption;
    protected int idleConsumption;
    protected int offConsumption;
    @Getter
    protected final DeviceId id;
    @Getter
    @Setter
    protected DeviceStatus status;
    @Setter
    protected DeviceTracker tracker;
    @Getter
    @Setter
    protected int timeActive;
    //add device documentation

    protected ElectronicDevice(int activeConsumption, int idleConsumption, DeviceId id) {
        this.activeConsumption = activeConsumption;
        this.idleConsumption = idleConsumption;
        this.offConsumption = 0;
        this.id = id;
        this.status = new Idle(this);
    }

    public void attach(DeviceBrokenEvent event) {
        super.attach(event);
        this.brokenEvent = event;
    }

    public void deviceBreakDown() {
        if (timeActive >= 0) {
            timeActive = -1;
        }
        status = new Broken(this);
        brokenEvent.update();
    }

    public void fix() {
        if (timeActive < 0) {
            this.status = new Idle(this);
        }
    }

    public void setActive() {
        this.status = new Active(this);
        timeActive = 1;
    }

    public void notifyToFix() {
        int randomHuman = Math.abs(r.nextInt() % House.getInstance().getAdultList().size());
        Human human = House.getInstance().getHumanList().get(randomHuman);
        human.setShouldRepairDevice(true);
        human.setDeviceToRepair(this);
    }

    @Override
    public String toString() {
        return id.getString() + " with tracker id " + tracker.getId();
    }
}

package smart_home.electronic_device;

import lombok.Getter;
import lombok.Setter;
import smart_home.utils.DeviceId;

public class Sunblind extends ElectronicDevice {

    @Getter
    @Setter
    private boolean isUp = true;

    public Sunblind() {
        super(2, 1, DeviceId.SUNBLIND);
    }


}

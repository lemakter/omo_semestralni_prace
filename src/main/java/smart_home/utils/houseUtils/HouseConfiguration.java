package smart_home.utils.houseUtils;

import lombok.*;
import smart_home.utils.DeviceId;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder //this is lombok builder (only a little bit cheating I guess xD)
// only adding this for the purpose of creating the configuration more easily
@NoArgsConstructor
public class HouseConfiguration {
    int floorNumber;
    List<Integer> roomNumbers;
    List<Integer> windowNumbers;
    int fatherNumber;
    int motherNumber;
    int childNumber;
    int catNumber;
    int dogNumber;
    int skiNumber;
    int bikeNumber;
    int carNumber;
    List<List<DeviceId>> devicesByRoom;
}

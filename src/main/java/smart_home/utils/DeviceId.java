package smart_home.utils;

import lombok.Getter;

public enum DeviceId {

    CD_PLAYER("CD player"),
    COMPUTER("computer"),
    DRYER("dryer"),
    FRIDGE("fridge"),
    GAS_BOILER("gas boiler"),
    MICROWAVE("microwave"),
    PHONE("phone"),
    SUNBLIND("sun-blind"),
    TV("TV"),
    WASHING_MACHINE("washing machine");

    @Getter
    private final String string;

    DeviceId(String string) {
        this.string = string;
    }
}

package smart_home.utils;

import camundajar.impl.com.google.gson.Gson;
import smart_home.House;
import smart_home.utils.builders.HouseBuilder;
import smart_home.utils.houseUtils.HouseConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputParser {

    private static Logger logger = Logger.getLogger(InputParser.class.getName());

    private InputParser() {
    }

    public static House parseFile(String filePath) {
        House house = null;
        try {
            String content = Files.readString(Paths.get(filePath));
            Gson gson = new Gson();
            HouseConfiguration houseConfiguration = gson.fromJson(content, HouseConfiguration.class);
            house = new HouseBuilder().fromConfig(houseConfiguration).build();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "File not found.");
        }
        return house;
    }


}

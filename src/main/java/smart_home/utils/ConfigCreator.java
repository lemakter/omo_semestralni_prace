package smart_home.utils;

import camundajar.impl.com.google.gson.Gson;
import smart_home.utils.houseUtils.HouseConfiguration;

import java.io.*;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigCreator {
    private static final Logger logger = Logger.getLogger(ConfigCreator.class.getName());

    public static void main(String[] args) {
        String filePath = "config02.txt";
        HouseConfiguration configuration = HouseConfiguration.builder()
                .catNumber(2)
                .dogNumber(1)
                .childNumber(5)
                .motherNumber(1)
                .fatherNumber(1)
                .skiNumber(1)
                .carNumber(1)
                .bikeNumber(2)
                .floorNumber(3)
                .roomNumbers(Arrays.asList(1, 3, 2)) //size needs to correspond with floor numbers
                .windowNumbers(Arrays.asList(2, 1, 1, 2, 2, 2))//size needs to correspond with number of rooms
                .devicesByRoom(Arrays.asList(
                        Arrays.asList(DeviceId.CD_PLAYER, DeviceId.COMPUTER),
                        Arrays.asList(DeviceId.DRYER, DeviceId.WASHING_MACHINE, DeviceId.PHONE),
                        Arrays.asList(DeviceId.FRIDGE, DeviceId.MICROWAVE, DeviceId.GAS_BOILER),
                        Arrays.asList(DeviceId.PHONE, DeviceId.TV),
                        Arrays.asList(DeviceId.PHONE, DeviceId.PHONE, DeviceId.CD_PLAYER),
                        Arrays.asList(DeviceId.TV, DeviceId.COMPUTER)))
                .build();

        Gson gson = new Gson();
        String jsonString = gson.toJson(configuration);
        File file = new File(filePath);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(jsonString);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.log(Level.INFO, () -> "Config file saved to " + file.getAbsolutePath());
    }

}

package smart_home.utils.builders;

import lombok.NoArgsConstructor;
import smart_home.House;
import smart_home.Room;
import smart_home.Window;
import smart_home.electronic_device.Sunblind;
import smart_home.electronic_device.api.SunblindAPI;
import smart_home.events.DeviceBrokenEvent;
import smart_home.utils.DeviceId;
import smart_home.utils.factories.DeviceFactory;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class RoomBuilder {
    Room room = new Room();
    DeviceFactory factory = new DeviceFactory();

    public RoomBuilder withId(int id) {
        room.setId(id);
        return this;
    }

    public RoomBuilder addWindows(int windowNumber) {
        List<Window> windowList = new ArrayList<>();
        for (int i = 0; i < windowNumber; i++) {
            Window window = new Window();
            Sunblind sunblind = new Sunblind();
            new DeviceBrokenEvent(sunblind);
            House.getInstance().getTrackerSystem().attachTracker(sunblind);
            window.setSunblind(sunblind);
            room.getDevicesList().add(new SunblindAPI(sunblind));
            windowList.add(window);
        }
        room.setWindowList(windowList);
        return this;
    }

    public RoomBuilder withDevices(List<DeviceId> devices) {
        for (DeviceId id : devices)
            switch (id) {
                case CD_PLAYER:
                    room.getDevicesList().add(factory.createCdPlayer());
                    break;
                case COMPUTER:
                    room.getDevicesList().add(factory.createComputer());
                    break;
                case DRYER:
                    room.getDevicesList().add(factory.createDryer());
                    break;
                case FRIDGE:
                    room.getDevicesList().add(factory.createFridge());
                    break;
                case GAS_BOILER:
                    room.getDevicesList().add(factory.createGasBoiler());
                    break;
                case MICROWAVE:
                    room.getDevicesList().add(factory.createMicrowave());
                    break;
                case PHONE:
                    room.getDevicesList().add(factory.createPhone());
                    break;
                case TV:
                    room.getDevicesList().add(factory.createTv());
                    break;
                case WASHING_MACHINE:
                    room.getDevicesList().add(factory.createWashingMachine());
                    break;
                case SUNBLIND:
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + id);
            }
        return this;
    }

    public Room build() {
        return room;
    }
}

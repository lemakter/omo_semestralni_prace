package smart_home.utils.builders;

import lombok.NoArgsConstructor;
import smart_home.Floor;
import smart_home.House;
import smart_home.Room;
import smart_home.events.CircuitBreakerEvent;
import smart_home.events.WindChangeEvent;
import smart_home.human.Father;
import smart_home.human.Human;
import smart_home.human.Kid;
import smart_home.human.Mother;
import smart_home.pet.Cat;
import smart_home.pet.Dog;
import smart_home.pet.Pet;
import smart_home.sports_equipment.SportsEquipment;
import smart_home.utils.DeviceId;
import smart_home.utils.houseUtils.HouseConfiguration;
import smart_home.sports_equipment.Bike;
import smart_home.Car;
import smart_home.sports_equipment.Ski;
import smart_home.utils.iterators.Iterator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@NoArgsConstructor
public class HouseBuilder {
    House house = House.getInstance();
    private static final int DEF_CAR_CONSUMPTION = 8;
    private final Random r = new Random();

    public HouseBuilder addFloors(int floorNumber, List<Integer> roomNumbers, List<Integer> windowNumbers, List<List<DeviceId>> devicesByRoom) {
        List<Floor> floorList = new ArrayList<>();
        int counter = 0;
        for (int i = 0; i < floorNumber; i++) {
            List<Room> roomList = new ArrayList<>();
            for (int j = 0; j < roomNumbers.get(i); j++) {
                roomList.add(new RoomBuilder()
                        .withId(counter)
                        .addWindows(windowNumbers.get(counter))
                        .withDevices(devicesByRoom.get(counter))
                        .build());
                counter++;
            }
            floorList.add(new Floor(roomList));
        }
        house.setFloorList(floorList);
        return this;
    }

    public HouseBuilder addPeople(int fatherNumber, int motherNumber, int childNumber) {
        List<Human> humanList = new ArrayList<>();
        for (int i = 0; i < fatherNumber; i++) {
            humanList.add(new Father());
            house.setFatherCount(house.getFatherCount() + 1);
        }
        for (int i = 0; i < motherNumber; i++) {
            humanList.add(new Mother());
            house.setMotherCount(house.getMotherCount() + 1);
        }
        for (int i = 0; i < childNumber; i++) {
            humanList.add(new Kid());
            house.setKidCount(house.getKidCount() + 1);
        }
        house.setHumanList(humanList);
        //set people in random rooms
        for (Iterator it = house.getHumanIterator(); it.hasNext(); ) {
            Human human = (Human) it.next();
            house.getTrackerSystem().attachTracker(human);
            int roomRandom = Math.abs(r.nextInt() % getRooms(house).size());
            human.setOccupying(getRooms(house).get(roomRandom));
            getRooms(house).get(roomRandom).getHumansCurrentlyInside().add(human);
        }
        return this;
    }

    public List<Room> getRooms(House house) {
        List<Room> roomList = new ArrayList<>();
        for (Floor floor : house.getFloorList()) {
            roomList.addAll(floor.getRoomList());
        }
        return roomList;
    }

    public HouseBuilder addPets(int catNumber, int dogNumber) {
        List<Pet> petList = new ArrayList<>();
        for (int i = 0; i < catNumber; i++) {
            Cat cat = new Cat();
            petList.add(cat);
            house.setCatCount(house.getCatCount() + 1);
            house.getTrackerSystem().attachTracker(cat);
            int roomRandom = Math.abs(r.nextInt() % getRooms(house).size());
            cat.setOccupying(getRooms(house).get(roomRandom));
        }
        for (int i = 0; i < dogNumber; i++) {
            Dog dog = new Dog();
            petList.add(dog);
            house.setDogCount(house.getDogCount() + 1);
            house.getTrackerSystem().attachTracker(dog);
            int roomRandom = Math.abs(r.nextInt() % getRooms(house).size());
            dog.setOccupying(getRooms(house).get(roomRandom));
        }
        house.setPetList(petList);
        return this;
    }

    public HouseBuilder withEquipment(int skiNumber, int bikeNumber) {
        List<SportsEquipment> equipmentList = new ArrayList<>();
        for (int i = 0; i < skiNumber; i++) {
            Ski ski = new Ski();
            house.getTrackerSystem().attachTracker(ski);
            equipmentList.add(ski);
            house.setSkiCount(house.getSkiCount() + 1);
        }
        for (int i = 0; i < bikeNumber; i++
        ) {
            Bike bike = new Bike();
            house.getTrackerSystem().attachTracker(bike);
            equipmentList.add(bike);
            house.setBikeCount(house.getBikeCount() + 1);
        }
        house.setSportsEquipmentList(equipmentList);
        return this;
    }

    public HouseBuilder withCars(int carNumber) {
        List<Car> carList = new ArrayList<>();
        for (int i = 0; i < carNumber; i++) {
            Car car = new Car(DEF_CAR_CONSUMPTION, i + 1);
            house.getTrackerSystem().attachTracker(car);
            carList.add(car);
        }
        house.setCarList(carList);
        return this;
    }

    public HouseBuilder addObservers() {
        house.attach(new WindChangeEvent(house));
        house.attach(new CircuitBreakerEvent(house));
        return this;
    }

    public HouseBuilder fromConfig(HouseConfiguration houseConfiguration) {
        addFloors(houseConfiguration.getFloorNumber(), houseConfiguration.getRoomNumbers(),
                houseConfiguration.getWindowNumbers(), houseConfiguration.getDevicesByRoom());
        addPeople(houseConfiguration.getFatherNumber(), houseConfiguration.getMotherNumber(),
                houseConfiguration.getChildNumber());
        addPets(houseConfiguration.getCatNumber(), houseConfiguration.getDogNumber());
        withCars(houseConfiguration.getCarNumber());
        withEquipment(houseConfiguration.getSkiNumber(), houseConfiguration.getBikeNumber());
        addObservers();
        return this;
    }

    public House build() {
        return house;
    }
}

package smart_home.utils.iterators;

import smart_home.Floor;
import smart_home.Room;
import smart_home.electronic_device.api.ElectronicDeviceAPI;

import java.util.ArrayList;
import java.util.List;

public class DeviceIterator implements Iterator {

    List<Floor> floorList;
    List<ElectronicDeviceAPI> deviceList = new ArrayList<>();
    int index = 0;

    public DeviceIterator(List<Floor> floorList) {
        this.floorList = floorList;
        for (Iterator it = new RoomIterator(floorList); it.hasNext(); ) {
            Room room = (Room) it.next();
            deviceList.addAll(room.getDevicesList());
        }
    }

    @Override
    public boolean hasNext() {
        return index < deviceList.size();
    }

    @Override
    public Object next() {
        if (hasNext()) {
            return deviceList.get(index++);
        }
        return null;
    }
}

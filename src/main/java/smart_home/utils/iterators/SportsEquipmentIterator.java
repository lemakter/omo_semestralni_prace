package smart_home.utils.iterators;

import smart_home.sports_equipment.SportsEquipment;

import java.util.List;

public class SportsEquipmentIterator implements Iterator {
    List<SportsEquipment> sportsEquipmentList;
    int index = 0;

    public SportsEquipmentIterator(List<SportsEquipment> sportsEquipmentList) {
        this.sportsEquipmentList = sportsEquipmentList;
    }

    @Override
    public boolean hasNext() {
        return index < sportsEquipmentList.size();
    }

    @Override
    public SportsEquipment next() {
        if (hasNext()) {
            return sportsEquipmentList.get(index ++);
        }
        return null;
    }
}

package smart_home.utils.iterators;

public interface Iterator {

    boolean hasNext();

    Object next();
}

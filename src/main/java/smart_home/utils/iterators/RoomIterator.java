package smart_home.utils.iterators;

import smart_home.Floor;
import smart_home.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomIterator implements Iterator {

    List<Floor> floorList;
    List<Room> roomList = new ArrayList<>();
    int index = 0;

    public RoomIterator(List<Floor> floorList) {
        this.floorList = floorList;
        for (Floor floor : floorList) {
            roomList.addAll(floor.getRoomList());
        }
    }

    @Override
    public boolean hasNext() {
        return index < roomList.size();
    }

    @Override
    public Object next() {
        if (hasNext()) {
            return roomList.get(index ++);
        }
        return null;
    }
}

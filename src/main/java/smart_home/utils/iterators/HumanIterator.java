package smart_home.utils.iterators;

import smart_home.human.Human;

import java.util.List;

public class HumanIterator implements Iterator {
    List<Human> humanList;
    int index = 0;

    public HumanIterator(List<Human> humanList) {
        this.humanList = humanList;
    }

    @Override
    public boolean hasNext() {
        return index < humanList.size();
    }

    @Override
    public Human next() {
        if (hasNext()) {
            return humanList.get(index ++);
        }
        return null;
    }
}

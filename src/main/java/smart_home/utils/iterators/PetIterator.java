package smart_home.utils.iterators;

import smart_home.pet.Pet;

import java.util.List;

public class PetIterator implements Iterator {
    List<Pet> petList;
    int index = 0;

    public PetIterator(List<Pet> petList) {
        this.petList = petList;
    }

    @Override
    public boolean hasNext() {
        return index < petList.size();
    }

    @Override
    public Pet next() {
        if (hasNext()) {
            return petList.get(index ++);
        }
        return null;
    }
}

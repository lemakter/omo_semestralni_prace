package smart_home.utils.iterators;

public interface Container {

    Iterator getHumanIterator();

    Iterator getPetIterator();

    Iterator getRoomIterator();

    Iterator getSportsEquipmentIterator();

    Iterator getElectronicDeviceIterator();
}

package smart_home.utils.factories;

import lombok.NoArgsConstructor;
import smart_home.House;
import smart_home.electronic_device.*;
import smart_home.electronic_device.api.*;
import smart_home.events.DeviceBrokenEvent;
import smart_home.events.FridgeOutOfFoodEvent;
import smart_home.events.GasLeakEvent;
import smart_home.events.WashingMachineFullEvent;


@NoArgsConstructor
public class DeviceFactory {

    House house = House.getInstance();

    public ElectronicDeviceAPI createCdPlayer() {
        CDPlayer player = new CDPlayer();
        house.getTrackerSystem().attachTracker(player);
        new DeviceBrokenEvent(player);
        return new ElectronicDeviceAPI(player);
    }

    public ElectronicDeviceAPI createComputer() {
        Computer computer = new Computer();
        house.getTrackerSystem().attachTracker(computer);
        new DeviceBrokenEvent(computer);
        return new ElectronicDeviceAPI(computer);
    }

    public ElectronicDeviceAPI createDryer() {
        Dryer dryer = new Dryer();
        house.getTrackerSystem().attachTracker(dryer);
        new DeviceBrokenEvent(dryer);
        return new DryerAPI(dryer);
    }

    public ElectronicDeviceAPI createFridge() {
        Fridge fridge = new Fridge();
        house.getTrackerSystem().attachTracker(fridge);
        new DeviceBrokenEvent(fridge);
        new FridgeOutOfFoodEvent(fridge);
        return new FridgeAPI(fridge);
    }

    public ElectronicDeviceAPI createGasBoiler() {
        GasBoiler boiler = new GasBoiler();
        house.getTrackerSystem().attachTracker(boiler);
        new DeviceBrokenEvent(boiler);
        new GasLeakEvent(boiler);
        return new GasBoilerAPI(boiler);
    }

    public ElectronicDeviceAPI createMicrowave() {
        Microwave microwave = new Microwave();
        house.getTrackerSystem().attachTracker(microwave);
        new DeviceBrokenEvent(microwave);
        return new ElectronicDeviceAPI(microwave);
    }

    public ElectronicDeviceAPI createPhone() {
        Phone phone = new Phone();
        house.getTrackerSystem().attachTracker(phone);
        new DeviceBrokenEvent(phone);
        return new ElectronicDeviceAPI(phone);
    }

    public ElectronicDeviceAPI createTv() {
        Tv tv = new Tv();
        house.getTrackerSystem().attachTracker(tv);
        new DeviceBrokenEvent(tv);
        return new ElectronicDeviceAPI(tv);
    }

    public ElectronicDeviceAPI createWashingMachine() {
        WashingMachine washingMachine = new WashingMachine();
        house.getTrackerSystem().attachTracker(washingMachine);
        new DeviceBrokenEvent(washingMachine);
        new WashingMachineFullEvent(washingMachine);
        return new WashingMachineAPI(washingMachine);
    }

}

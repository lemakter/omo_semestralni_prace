package smart_home;

import smart_home.utils.InputParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String pathName = reader.readLine();
            House house = InputParser.parseFile(pathName);
            Controller controller = new Controller(house);
            controller.runHouse(1000);
            house.generateReports();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

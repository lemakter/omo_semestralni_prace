package smart_home.trackers;

import lombok.Getter;
import smart_home.electronic_device.ElectronicDevice;

public class DeviceTracker {

    private static final double GENERAL_ADDITIVE_CONSTANT = 0.2; //constant generating increase in energy consumption
    @Getter
    private final int id;
    @Getter
    private final ElectronicDevice device;
    private final int activeConsumption;
    private final int idleConsumption;
    private final int offConsumption;
    private int activeHours = 0;
    @Getter
    private int consumed = 0;

    public DeviceTracker(int id, ElectronicDevice device) {
        this.id = id;
        this.activeConsumption = device.getActiveConsumption();
        this.idleConsumption = device.getIdleConsumption();
        this.offConsumption = device.getOffConsumption();
        this.device = device;
    }

    public void trackActiveUsage() {
        ++activeHours;
        consumed += (activeConsumption + activeHours * GENERAL_ADDITIVE_CONSTANT);
    }

    public void trackIdleUsage() {
        consumed += (idleConsumption + activeHours * GENERAL_ADDITIVE_CONSTANT);
    }

    public void trackOffUsage() {
        consumed += (offConsumption + activeHours * GENERAL_ADDITIVE_CONSTANT);
    }

    public String getConsumptionReportLine() {
        return device.toString() + " consumed " + consumed + " units of power and had been used " + activeHours + " units of time.";
    }

}

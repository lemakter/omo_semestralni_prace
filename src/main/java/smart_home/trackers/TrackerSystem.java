package smart_home.trackers;

import lombok.Getter;
import lombok.NoArgsConstructor;
import smart_home.Car;
import smart_home.electronic_device.ElectronicDevice;
import smart_home.human.Human;
import smart_home.pet.Pet;
import smart_home.sports_equipment.SportsEquipment;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
public class TrackerSystem {


    private final List<PetTracker> petTrackers = new ArrayList<>();
    private final List<DeviceTracker> deviceTrackers = new ArrayList<>();
    private final List<ActivityTracker> activityTrackers = new ArrayList<>();
    private final List<EquipmentTracker> equipmentTrackers = new ArrayList<>();
    private final List<CarTracker> carTrackers = new ArrayList<>();
    private int counter = 0;

    public void attachTracker(ElectronicDevice device) {
        DeviceTracker tracker = new DeviceTracker(counter, device);
        deviceTrackers.add(tracker);
        device.setTracker(tracker);
        counter += 1;
    }

    public void attachTracker(Human human) {
        ActivityTracker tracker = new ActivityTracker(counter, human);
        activityTrackers.add(tracker);
        human.setTracker(tracker);
        counter++;
    }

    public void attachTracker(SportsEquipment equipment) {
        EquipmentTracker tracker = new EquipmentTracker(counter, equipment);
        equipmentTrackers.add(tracker);
        equipment.setTracker(tracker);
        counter++;
    }

    public void attachTracker(Car car) {
        CarTracker tracker = new CarTracker(counter, car.getConsumption());
        carTrackers.add(tracker);
        car.setTracker(tracker);
        counter++;
    }

    public void attachTracker(Pet pet) {
        PetTracker tracker = new PetTracker(counter, pet);
        petTrackers.add(tracker);
        pet.setTracker(tracker);
        counter++;
    }
}

package smart_home.trackers;

import smart_home.human.Human;

import java.util.HashMap;
import java.util.Map;

public class CarTracker {
    private final int id;

    private int fuelUsed = 0;
    private int distanceTraveled = 0;
    private final int consumption;
    private final Map<Human, Integer> usageByHumans = new HashMap<>();

    public CarTracker(int id, int consumption) {
        this.id = id;
        this.consumption = consumption;
    }

    public void trackFuelUsage(Human human, int distance) {
        if (human == null) return; //car needs to be operated bz human

        usageByHumans.putIfAbsent(human, 0);
        usageByHumans.put(human, usageByHumans.get(human) + 1);

        fuelUsed += (distance * consumption / 100); //consumption is per 100 distance unit
        distanceTraveled += distance;
    }

    public String getReportLine() {
        return "Car connected to tracker " + id + "used up " + fuelUsed +
                " units of fuel and traveled " + distanceTraveled + " units.";
    }
}

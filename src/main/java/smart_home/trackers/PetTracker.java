package smart_home.trackers;

import smart_home.Room;
import smart_home.pet.Pet;

import java.util.HashMap;
import java.util.Map;

public class PetTracker {

    private final Pet pet;
    private final int id;

    private final Map<Room, Integer> roomsVisited = new HashMap<>();

    public PetTracker(int id, Pet pet) {
        this.id = id;
        this.pet = pet;
    }

    public void trackRooms(Room occupying) {
        roomsVisited.putIfAbsent(occupying, 0);
        roomsVisited.put(occupying, roomsVisited.get(occupying) + 1);
    }

    public String generateReportLine() {
        StringBuilder sb = new StringBuilder(pet.toString());
        sb.append(" with index ")
                .append(id)
                .append(" spend: ")
                .append(System.lineSeparator());
        for (Map.Entry<Room, Integer> entry : roomsVisited.entrySet()) {
            sb.append("   ")
                    .append(entry.getValue())
                    .append(" time units in ")
                    .append(entry.getKey().toString())
                    .append(System.lineSeparator());
        }
        return sb.toString();
    }
}

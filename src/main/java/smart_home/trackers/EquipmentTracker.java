package smart_home.trackers;

import lombok.Getter;
import smart_home.human.Human;
import smart_home.sports_equipment.SportsEquipment;

import java.util.HashMap;
import java.util.Map;

public class EquipmentTracker {
    @Getter
    private final int id;
    private final SportsEquipment equipment;
    private int hoursUsed = 0;
    private final Map<Human, Integer> usageByHumans = new HashMap<>();

    public EquipmentTracker(int id, SportsEquipment equipment) {
        this.id = id;
        this.equipment = equipment;
    }

    public void trackUsage(Human human) {
        if (human == null) return;
        usageByHumans.putIfAbsent(human, 0);
        usageByHumans.put(human, usageByHumans.get(human) + 1);
        hoursUsed++;
    }

    public String generateReportLine() {
        return equipment.toString() +
                " had been used " + hoursUsed + " units of time.";
    }

}

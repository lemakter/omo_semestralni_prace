package smart_home.trackers;

import lombok.Getter;
import smart_home.Car;
import smart_home.electronic_device.ElectronicDevice;
import smart_home.human.Human;
import smart_home.sports_equipment.SportsEquipment;

import java.util.HashMap;
import java.util.Map;

public class ActivityTracker {
    private final long id;

    private final Map<ElectronicDevice, Integer> devicesByUsage = new HashMap<>();
    private final Map<SportsEquipment, Integer> equipmentByUsage = new HashMap<>();
    private final Map<Car, Integer> carByUsage = new HashMap<>();
    @Getter
    private int timeActive = 0;
    private final Human human;

    public ActivityTracker(long id, Human human) {
        this.id = id;
        this.human = human;
    }

    public void trackDeviceUsage(ElectronicDevice device) {
        devicesByUsage.putIfAbsent(device, 0);
        devicesByUsage.put(device, devicesByUsage.get(device) + 1);
        ++timeActive;
    }

    public void trackEquipmentUsage(SportsEquipment equipment) {
        equipmentByUsage.putIfAbsent(equipment, 0);
        equipmentByUsage.put(equipment, equipmentByUsage.get(equipment) + 1);
        ++timeActive;
    }

    public void trackCarUsage(Car car) {
        carByUsage.putIfAbsent(car, 0);
        carByUsage.put(car, carByUsage.get(car) + 1);
        ++timeActive;
    }

    public String getReportLine() {
        StringBuilder sb = new StringBuilder()
                .append(human.toString())
                .append(" with tracker ")
                .append(id)
                .append(" had spent:")
                .append(System.lineSeparator());
        for (Map.Entry<ElectronicDevice, Integer> entry: devicesByUsage.entrySet()) {
            sb
                    .append("   ")
                    .append(entry.getValue())
                    .append(" time units using ")
                    .append(entry.getKey().toString())
                    .append(System.lineSeparator());

        }
        for (Map.Entry<SportsEquipment, Integer> entry : equipmentByUsage.entrySet()) {
            sb
                    .append("   ")
                    .append(entry.getValue())
                    .append(" time units using ")
                    .append(entry.getKey().toString())
                    .append(System.lineSeparator());

        }
        for(Map.Entry<Car, Integer> entry: carByUsage.entrySet()){
            sb
            .append("   ")
                    .append(entry.getValue())
                    .append(" time units using ")
                    .append(entry.getKey().toString())
                    .append(System.lineSeparator());
        }
        return sb.toString();
    }

}

package smart_home.pet;

import lombok.Getter;
import lombok.Setter;
import smart_home.House;
import smart_home.Room;
import smart_home.trackers.PetTracker;

import java.util.Random;

public abstract class Pet {

    private static final Random r = new Random();

    @Setter
    protected Room occupying;
    @Getter
    @Setter
    protected PetTracker tracker;


    public void update() {
        tracker.trackRooms(occupying);
        House house = House.getInstance();
        int roomRandom = Math.abs(r.nextInt() % house.getRooms().size());
        this.occupying = house.getRooms().get(roomRandom);
    }
}

package smart_home.sports_equipment;

import lombok.Getter;
import lombok.Setter;
import smart_home.human.Human;
import smart_home.trackers.EquipmentTracker;

public abstract class SportsEquipment {
    @Getter
    private boolean isFree;
    private Human currentlyUsing;
    @Setter
    protected EquipmentTracker tracker;

    public boolean useEquipment(Human human) {
        if (!isFree) {
            //if equipment is already in use no one can use it at the same time
            return false;
        }
        isFree = false;
        currentlyUsing = human;
        human.setUsing(this);
        return true;
    }

    public void update() {
        if (!isFree) {
            tracker.trackUsage(currentlyUsing);
            isFree = true;
        }
    }
}

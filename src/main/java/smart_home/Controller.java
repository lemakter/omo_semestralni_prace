package smart_home;

import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.electronic_device.api.FridgeAPI;
import smart_home.electronic_device.api.GasBoilerAPI;
import smart_home.electronic_device.api.WashingMachineAPI;
import smart_home.human.Human;
import smart_home.sports_equipment.SportsEquipment;
import smart_home.utils.DeviceId;
import smart_home.utils.iterators.Iterator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Controller {

    private final House house;
    private final Random r = new Random();

    public Controller(House house) {
        this.house = house;
    }

    private void notifyFridgeOutOfFoodSensor() {
        for (Iterator it = house.getElectronicDeviceIterator(); it.hasNext(); ) {
            ElectronicDeviceAPI device = (ElectronicDeviceAPI) it.next();
            if (device.getId().equals(DeviceId.FRIDGE)) {
                ((FridgeAPI) device).setNoFood();
            }
        }
    }

    private void notifyGasLeakSensor() {
        for (Iterator it = house.getElectronicDeviceIterator(); it.hasNext(); ) {
            ElectronicDeviceAPI device = (ElectronicDeviceAPI) it.next();
            if (device.getId().equals(DeviceId.GAS_BOILER)) {
                ((GasBoilerAPI) device).triggerGasLeak();
            }
        }
    }

    private void notifyWindSensor() {
        house.getWindChangeEvent().update();
    }

    private void notifyWashingMachineFullSensor() {
        for (Iterator it = house.getElectronicDeviceIterator(); it.hasNext(); ) {
            ElectronicDeviceAPI device = (ElectronicDeviceAPI) it.next();
            if (device.getId().equals(DeviceId.WASHING_MACHINE)) {
                ((WashingMachineAPI) device).setFull();
            }
        }
    }

    private void notifyCircuitBreakerSensor() {
        house.getCircuitBreakerEvent().update();
    }

    private void generateEvents() {
        int random;
        int eventRandom;
        //generate events
        random = Math.abs(r.nextInt() % 10);
        if (random != 0) return;
        //something happens
        eventRandom = Math.abs(r.nextInt() % 10);
        switch (eventRandom) {
            case 0:
                //device breaks
                List<ElectronicDeviceAPI> devices = house.getAllDevices();
                int index = Math.abs(r.nextInt() % devices.size());
                devices.get(index).deviceBreakDown();
                break;
            case 1:
            case 2:
            case 3:
                notifyFridgeOutOfFoodSensor();
                break;
            case 4:
                notifyGasLeakSensor();
                break;
            case 5:
            case 6:
                notifyWindSensor();
                break;
            case 7:
            case 8:
                notifyWashingMachineFullSensor();
                break;
            case 9:
                notifyCircuitBreakerSensor();
                break;
            default:
                break;
        }
    }

    private Car findFreeCar() {
        for (Car car : house.getCarList()) {
            if (car.getCurrentlyDriving() == null) {
                return car;
            }
        }
        return null;
    }

    public void runHouse(int time) {
        for (int i = 0; i < time; i++) {
            generateEvents();
            house.update();
            //change what humans are doing
            for (Iterator it = house.getHumanIterator(); it.hasNext(); ) {
                decideHumanActivity((Human) it.next());
            }
        }
    }

    private void decideHumanActivity(Human human) {
        int sportTest = Math.abs(r.nextInt() % 10);
        int roomChangeTest = Math.abs(r.nextInt() % 4);
        if (!house.getSportsEquipmentList().isEmpty() && sportTest <= 4) {
            //do sports with 50 % probability
            SportsEquipment sportsEquipment = house.getSportsEquipmentList()
                    .get(Math.abs(r.nextInt() % house.getSportsEquipmentList().size()));
            human.setUsing(sportsEquipment);
            sportsEquipment.useEquipment(human);
        } else if (human.isAdult() && roomChangeTest == 0) {
            //use car with 25 % probability
            Car car = findFreeCar();
            if (car != null)
                car.drive(human, Math.abs(r.nextInt() % 100));
        } else if (roomChangeTest <= 2) {
            //change room with 50 % probability
            human.changeRooms(house.getRooms()
                    .get(Math.abs(r.nextInt() % house.getRooms().size())));
            //use device
            List<ElectronicDeviceAPI> usableDevices = getUsableDevices(human.getOccupying().getDevicesList());
            if (!usableDevices.isEmpty()) {
                usableDevices.get(Math.abs(r.nextInt() % usableDevices.size())).useDevice(human);
            }
        }
    }


    public List<ElectronicDeviceAPI> getUsableDevices(List<ElectronicDeviceAPI> deviceList) {
        List<ElectronicDeviceAPI> usableDevices = new ArrayList<>();
        for (ElectronicDeviceAPI device : deviceList) {
            switch (device.getId()) {
                case COMPUTER:
                case PHONE:
                case CD_PLAYER:
                case TV:
                case MICROWAVE:
                    usableDevices.add(device);
                    break;
                default:
                    break;
            }
        }
        return usableDevices;
    }

}

package smart_home;

import lombok.Getter;
import lombok.Setter;
import smart_home.human.Human;
import smart_home.trackers.CarTracker;

public class Car {

    private final int id;
    @Setter
    private CarTracker tracker;
    @Getter
    private final int consumption;
    @Getter
    private Human currentlyDriving;
    private int distanceToGo;

    public Car(int consumption, int id) {
        this.consumption = consumption;
        this.id = id;
    }

    public boolean drive(Human driver, int distance) {
        if (distance>0 && distanceToGo <= 0) {
            this.distanceToGo = distance;
            currentlyDriving = driver;
            driver.setUsing(this);
            return true;
        }
        return false;
    }

    public void update() {
        tracker.trackFuelUsage(currentlyDriving, distanceToGo);
        distanceToGo = 0;
        currentlyDriving = null;
    }

    @Override
    public String toString() {
        return "car n." + id;
    }

}

package smart_home;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.human.Human;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class Room {
    private List<Window> windowList;
    private final List<ElectronicDeviceAPI> devicesList = new ArrayList<>();
    private final List<Human> humansCurrentlyInside = new ArrayList<>();
    private int id;

    @Override
    public String toString() {
        return "room " + id;
    }

}

package smart_home.report;

import smart_home.House;
import smart_home.trackers.ActivityTracker;
import smart_home.trackers.PetTracker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;

public class ActivityAndUsageReport extends Report {

    private static final String FILE_PATH = "activityAndUsageReport.txt";

    @Override
    public void generate(House house) {
        StringBuilder sb = new StringBuilder("Usage and activity report: ")
                .append(System.lineSeparator())
                .append("Human activity:")
                .append(System.lineSeparator());
        for (ActivityTracker tracker : house.getTrackerSystem().getActivityTrackers()) {
            sb.append(tracker.getReportLine());
        }
        sb.append(System.lineSeparator())
                .append("Pet activity:")
                .append(System.lineSeparator());
        for (PetTracker tracker : house.getTrackerSystem().getPetTrackers()) {
            sb.append(tracker.generateReportLine());
        }
        File file = new File(FILE_PATH);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(sb.toString());
        } catch (IOException e) {
            logger.log(Level.WARNING, "Unable to save activity and usage report");
        }
        logger.log(Level.INFO, () -> "Activity and usage report saved to " + file.getAbsolutePath());

    }
}

package smart_home.report;

import smart_home.House;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;


public class EventReport extends Report {

    private static final String FILE_NAME = "eventReport.txt";

    @Override
    public void generate(House house) {
        File file = new File(FILE_NAME);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(house.getEventLogger().toString());
        } catch (IOException e) {
            logger.log(Level.WARNING, "Unable to save event report");
        }
        logger.log(Level.INFO, () -> "Event report saved to " + file.getAbsolutePath());
    }
}

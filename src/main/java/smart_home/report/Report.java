package smart_home.report;

import smart_home.House;

import java.util.logging.Logger;

public abstract class Report {

    protected static final Logger logger = Logger.getLogger(Report.class.getName());

    public abstract void generate(House house);
}

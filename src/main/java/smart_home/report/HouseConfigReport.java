package smart_home.report;

import smart_home.Floor;
import smart_home.House;
import smart_home.Room;
import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.utils.DeviceId;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class HouseConfigReport extends Report {

    private static final String FILE_PATH = "houseConfigReport.txt";


    private void addDeviceStrings(StringBuilder sb, Room room) {
        List<ElectronicDeviceAPI> devices = getDevices(room);
        int tempCounter = 0;
        for (ElectronicDeviceAPI device : devices) {
            if (tempCounter != 0) {
                sb.append(", ");
            }
            sb.append(device.toString())
                    .append(" ");
            tempCounter++;
        }
        sb.append(System.lineSeparator());
    }

    @Override
    public void generate(House house) {
        List<Floor> floorList = house.getFloorList();
        StringBuilder sb = new StringBuilder();
        sb.append("House configuration report:")
                .append(System.lineSeparator())
                .append("House has ")
                .append(house.getFloorList().size())
                .append(" floors.");
        sb.append(System.lineSeparator());
        for (int i = 0; i < floorList.size(); i++) {
            int roomCount;
            roomCount = floorList.get(i).getRoomList().size();
            sb.append(i + 1)
                    .append(". floor has ")
                    .append(roomCount)
                    .append(" rooms.")
                    .append(System.lineSeparator());
        }
        for (int i = 0; i < floorList.size(); i++) {
            for (int j = 0; j < floorList.get(i).getRoomList().size(); j++) {
                Room room = floorList.get(i).getRoomList().get(j);
                sb.append(j + 1)
                        .append(". room on ")
                        .append(i + 1)
                        .append(". floor has ")
                        .append(room.getWindowList().size())
                        .append(" windows and current devices: ");
                sb.append(System.lineSeparator());
                addDeviceStrings(sb, room);
            }
        }
        sb.append("Residents of the house are:")
                .append(System.lineSeparator())
                .append(house.getFatherCount())
                .append(" father");
        if (house.getFatherCount() != 1)
            sb.append("s");
        sb.append(System.lineSeparator())
                .append(house.getMotherCount())
                .append(" mother");
        if (house.getMotherCount() != 1)
            sb.append("s");
        sb.append(System.lineSeparator())
                .append(house.getKidCount())
                .append(" kid");
        if (house.getKidCount() != 1)
            sb.append("s");
        sb.append(System.lineSeparator())
                .append(house.getCatCount())
                .append(" cat");
        if (house.getCatCount() != 1)
            sb.append("s");
        sb.append(System.lineSeparator())
                .append(house.getDogCount())
                .append(" dog");
        if (house.getDogCount() != 1)
            sb.append("s");
        sb.append(System.lineSeparator())
                .append("The house also has: ")
                .append(System.lineSeparator())
                .append(house.getSkiCount())
                .append(" ski");
        if (house.getSkiCount() != 1)
            sb.append("s");
        sb.append(System.lineSeparator())
                .append(house.getBikeCount())
                .append(" bike");
        if (house.getBikeCount() != 1)
            sb.append("s");
        sb.append(System.lineSeparator())
                .append(house.getCarList().size())
                .append(" car");
        if (house.getCarList().size() != 1)
            sb.append("s");
        sb.append(System.lineSeparator());
        File file = new File(FILE_PATH);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(sb.toString());
            bw.flush();
        } catch (IOException e) {
            logger.log(Level.WARNING, "Unable to save house config report.");
        }
        logger.log(Level.INFO, () -> "House config report saved to " + file.getAbsolutePath());
    }

    public List<ElectronicDeviceAPI> getDevices(Room room) {
        List<ElectronicDeviceAPI> devicesWithoutSunblinds = new ArrayList<>();
        for (ElectronicDeviceAPI device : room.getDevicesList()) {
            if (!device.getId().equals(DeviceId.SUNBLIND)) {
                devicesWithoutSunblinds.add(device);
            }
        }
        return devicesWithoutSunblinds;
    }
}

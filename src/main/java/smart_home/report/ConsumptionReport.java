package smart_home.report;

import smart_home.House;
import smart_home.trackers.DeviceTracker;
import smart_home.utils.DeviceId;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.logging.Level;


public class ConsumptionReport extends Report {
    private static final String FILE_PATH = "consuptionReport.txt";

    @Override
    public void generate(House house) {
        StringBuilder sb = new StringBuilder("Consumption report: ")
                .append(System.lineSeparator())
                .append("Device energy consumption:")
                .append(System.lineSeparator());
        int consumption = 0;
        int blindConsuption = 0;
        int blindCounter = 0;
        for (DeviceTracker tracker : house.getTrackerSystem().getDeviceTrackers()) {
            if (tracker.getDevice().getId().equals(DeviceId.SUNBLIND)) {
                blindConsuption += tracker.getConsumed();
                blindCounter++;
            } else {
                sb.append("   ")
                        .append(tracker.getConsumptionReportLine())
                        .append(System.lineSeparator());
            }
            consumption += tracker.getConsumed();
        }

        sb.append("   ")
                .append(blindCounter)
                .append(" ")
                .append(DeviceId.SUNBLIND.getString());
        if (blindCounter != 1) {
            sb.append("s");
        }
        sb.append(" altogether consumed ")
                .append(blindConsuption)
                .append(" units of power.")
                .append(System.lineSeparator())

                .append("The household consumed in total ")
                .append(consumption)
                .append(" units of power.")
                .append(System.lineSeparator())
                .append("With cost ")
                .append(House.getELECTRICITY_COST_PER_UNIT())
                .append(" units of money per unit of power, the total cost of consumed power is ")
                .append(House.getELECTRICITY_COST_PER_UNIT().multiply(BigDecimal.valueOf(consumption)))
                .append(" units of money.");

        File file = new File(FILE_PATH);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(sb.toString());
        } catch (IOException e) {
            logger.log(Level.WARNING, "Unable to save consumption report");
        }
        logger.log(Level.INFO, () -> "Consumption report saved to " + file.getAbsolutePath());

    }
}

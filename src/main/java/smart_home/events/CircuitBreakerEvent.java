package smart_home.events;

import smart_home.House;
import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.electronic_device.Sensor;
import smart_home.utils.DeviceId;

import java.util.List;

public class CircuitBreakerEvent implements EventObserver {

    private final Sensor sensor;
    private final House house = House.getInstance();

    public CircuitBreakerEvent(House sensor) {
        this.sensor = sensor;
        sensor.attach(this);
    }

    @Override
    public void update() {
        //turn off some devices
        List<ElectronicDeviceAPI> devices = house.getAllDevices();
        for (ElectronicDeviceAPI device : devices) {
            if (device.getId().equals(DeviceId.CD_PLAYER) ||
                    device.getId().equals(DeviceId.MICROWAVE) ||
                    device.getId().equals(DeviceId.SUNBLIND) ||
                    device.getId().equals(DeviceId.WASHING_MACHINE)) {
                device.deviceBreakDown();
            }
        }
        house.getEventLogger().logEvent(this);
    }

    @Override
    public String toString() {
        return "Circuit breaker event was generated, all non-essential devices were turned off.";
    }
}

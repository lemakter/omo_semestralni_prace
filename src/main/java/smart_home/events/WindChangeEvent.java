package smart_home.events;

import smart_home.House;
import smart_home.electronic_device.Sensor;
import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.electronic_device.api.SunblindAPI;
import smart_home.utils.DeviceId;

import java.util.List;

public class WindChangeEvent implements EventObserver {

    private final Sensor sensor;
    private final House house = House.getInstance();

    public WindChangeEvent(House sensor) {
        this.sensor = sensor;
        sensor.attach(this);
    }

    @Override
    public void update() {
        List<ElectronicDeviceAPI> devices = house.getAllDevices();
        for (ElectronicDeviceAPI device : devices) {
            if (device.getId().equals(DeviceId.SUNBLIND)) {
                ((SunblindAPI) device).riseBlind();
            }
        }
        house.getEventLogger().logEvent(this);
    }

    @Override
    public String toString() {
        return "Wind change event was generated, all sun-blinds had been risen.";
    }
}

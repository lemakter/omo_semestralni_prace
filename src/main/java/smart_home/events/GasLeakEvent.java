package smart_home.events;


import smart_home.House;
import smart_home.electronic_device.GasBoiler;
import smart_home.human.Father;
import smart_home.human.Human;
import smart_home.electronic_device.Sensor;
import smart_home.utils.iterators.Iterator;

public class GasLeakEvent implements EventObserver {

    private final Sensor sensor;
    private final House house = House.getInstance();
    private String humanString;

    public GasLeakEvent(GasBoiler sensor) {
        this.sensor = sensor;
        sensor.attach(this);
    }

    @Override
    public void update() {
        //make father to turn off the gas boiler
        for (Iterator it = house.getHumanIterator(); it.hasNext(); ) {
            Human human = (Human) it.next();
            if (human instanceof Father father) {
                father.turnOffGasBoiler();
                humanString = father.toString();
                house.getEventLogger().logEvent(this);
            }
        }
    }

    @Override
    public String toString(){
        return "Gas leak event was generated so " + sensor.toString() + " was turned off by "+humanString+".";
    }
}

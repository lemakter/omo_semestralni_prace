package smart_home.events;

import smart_home.House;
import smart_home.electronic_device.Sensor;

public interface EventObserver {

    public void update();
}

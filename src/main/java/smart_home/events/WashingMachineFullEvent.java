package smart_home.events;

import smart_home.House;
import smart_home.electronic_device.*;

public class WashingMachineFullEvent implements EventObserver {

    private final Sensor sensor;
    private final House house = House.getInstance();

    public WashingMachineFullEvent(WashingMachine sensor) {
        this.sensor = sensor;
        sensor.attach(this);
    }

    @Override
    public void update() {
        ((WashingMachine) sensor).useDevice();
        house.getEventLogger().logEvent(this);
    }

    @Override
    public String toString() {
        return "Washing machine full event was generated and " + sensor.toString() + "was turned on.";
    }
}

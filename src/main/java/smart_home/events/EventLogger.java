package smart_home.events;


public class EventLogger {
    private final StringBuilder sb = new StringBuilder();
    private int fridgeCounter = 0;
    private int circuitBreakCounter = 0;
    private int gasLeakCounter = 0;
    private int washingMachineCounter = 0;
    private int windCounter = 0;

    public void logEvent(DeviceBrokenEvent event) {
        sb.append(event.toString())
                .append(System.lineSeparator());
    }

    public void logEvent(CircuitBreakerEvent event) {
        circuitBreakCounter++;
    }

    public void logEvent(FridgeOutOfFoodEvent event) {
        fridgeCounter++;
    }

    public void logEvent(GasLeakEvent event) {
        gasLeakCounter++;

    }

    public void logEvent(WashingMachineFullEvent event) {
        washingMachineCounter++;
    }

    public void logEvent(WindChangeEvent event) {
        windCounter++;
    }

    @Override
    public String toString() {
        return "Circuit breaker event was generated " +
                circuitBreakCounter +
                " times, all non-essential devices were turned off." +
                System.lineSeparator() +
                "Fridge out of food event was generated " +
                fridgeCounter +
                " times." +
                System.lineSeparator() +
                "Gas leak event was generated " +
                gasLeakCounter +
                " times." +
                System.lineSeparator() +
                "Washing machine full event was generated " +
                washingMachineCounter +
                " times and was resolved automatically." +
                System.lineSeparator() +
                "Wind change event was generated " +
                windCounter +
                " times, each time all sun-blinds had been risen." +
                System.lineSeparator() +
                sb;
    }


}

package smart_home.events;

import smart_home.House;
import smart_home.electronic_device.Fridge;
import smart_home.human.Human;
import smart_home.human.Mother;
import smart_home.electronic_device.Sensor;
import smart_home.utils.iterators.Iterator;

public class FridgeOutOfFoodEvent implements EventObserver {

    private final Sensor sensor;
    private final House house = House.getInstance();
    private String humanString;

    public FridgeOutOfFoodEvent(Fridge sensor) {
        this.sensor = sensor;
        sensor.attach(this);
    }

    @Override
    public void update() {
        //send mother to buy food
        for (Iterator it = house.getHumanIterator(); it.hasNext(); ) {
            Human human = (Human) it.next();
            if (human instanceof Mother mother) {
                mother.goBuyFood();
                humanString = mother.toString();
                house.getEventLogger().logEvent(this);
                break;
            }
        }
    }

    @Override
    public String toString() {
        return "Fridge out of food event was resolved by " + humanString + ".";
    }
}

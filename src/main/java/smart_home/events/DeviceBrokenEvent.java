package smart_home.events;

import smart_home.House;
import smart_home.electronic_device.ElectronicDevice;
import smart_home.electronic_device.Sensor;
import smart_home.human.Human;

public class DeviceBrokenEvent implements EventObserver {

    private final Sensor sensor;
    private final House house = House.getInstance();
    private String humanString;

    public DeviceBrokenEvent(ElectronicDevice sensor) {
        this.sensor = sensor;
        sensor.attach(this);
    }

    @Override
    public void update() {
        for (Human human : house.getAdultList()) {
            if (!human.isShouldRepairDevice()) {
                human.setShouldRepairDevice(true);
                human.setDeviceToRepair((ElectronicDevice) sensor);
                humanString = human.toString();
                house.getEventLogger().logEvent(this);
                break;
            }
        }
    }

    @Override
    public String toString() {
        return "Device " + sensor.toString()
                + " broke down. Event was resolved by " + humanString;
    }
}

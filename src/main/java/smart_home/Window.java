package smart_home;

import lombok.Setter;
import smart_home.electronic_device.Sunblind;

@Setter
public class Window {
    private Sunblind sunblind;
}

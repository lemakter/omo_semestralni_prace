package smart_home;

import lombok.Getter;
import lombok.Setter;
import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.events.CircuitBreakerEvent;
import smart_home.events.EventLogger;
import smart_home.events.WindChangeEvent;
import smart_home.human.Human;
import smart_home.pet.Pet;
import smart_home.report.*;
import smart_home.electronic_device.Sensor;
import smart_home.sports_equipment.SportsEquipment;
import smart_home.trackers.TrackerSystem;
import smart_home.utils.iterators.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class House extends Sensor implements Container {

    @Getter
    private static final BigDecimal ELECTRICITY_COST_PER_UNIT = BigDecimal.valueOf(1.2);
    private static House INSTANCE;

    private WindChangeEvent windChangeEvent;
    private CircuitBreakerEvent circuitBreakerEvent;

    private EventLogger eventLogger = new EventLogger();
    private TrackerSystem trackerSystem = new TrackerSystem();
    private List<Floor> floorList;
    private List<Human> humanList;
    private List<Pet> petList;
    private List<SportsEquipment> sportsEquipmentList;
    private List<Car> carList;
    private int fatherCount = 0;
    private int motherCount = 0;
    private int kidCount = 0;
    private int catCount = 0;
    private int dogCount = 0;
    private int skiCount = 0;
    private int bikeCount = 0;


    public List<Human> getAdultList() {
        List<Human> adultsList = new ArrayList<>();
        for (Human human : humanList) {
            if (human.isAdult())
                adultsList.add(human);
        }
        return adultsList;
    }

    public void generateReports() {
        ActivityAndUsageReport activityAndUsageReport = new ActivityAndUsageReport();
        ConsumptionReport consumptionReport = new ConsumptionReport();
        EventReport eventReport = new EventReport();
        HouseConfigReport houseConfigReport = new HouseConfigReport();
        activityAndUsageReport.generate(this);
        consumptionReport.generate(this);
        eventReport.generate(this);
        houseConfigReport.generate(this);
    }

    public static House getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new House();
        }
        return INSTANCE;
    }

    public List<ElectronicDeviceAPI> getAllDevices() {
        List<ElectronicDeviceAPI> activeDevices = new ArrayList<>();
        for (Floor floor : floorList) {
            for (Room room : floor.roomList) {
                activeDevices.addAll(room.getDevicesList());
            }
        }
        return activeDevices;
    }

    private void updateHumans() {
        Iterator it = getHumanIterator();
        while (it.hasNext()) {
            Human human = (Human) it.next();
            human.update();
        }
    }

    private void updateDevices() {
        Iterator it = getElectronicDeviceIterator();
        while (it.hasNext()) {
            ElectronicDeviceAPI device = (ElectronicDeviceAPI) it.next();
            device.update();
        }
    }

    private void updatePets() {
        Iterator it = getPetIterator();
        while (it.hasNext()) {
            Pet pet = (Pet) it.next();
            pet.update();
        }
    }

    private void updateEquipment() {
        for (SportsEquipment equipment : sportsEquipmentList) {
            equipment.update();
        }
    }

    private void updateCars() {
        for (Car car : carList) {
            car.update();
        }
    }

    public List<Room> getRooms() {
        List<Room> roomList = new ArrayList<>();
        for (Floor floor : getFloorList()) {
            roomList.addAll(floor.roomList);
        }
        return roomList;
    }

    public void update() {
        updateEquipment();
        updateDevices();
        updatePets();
        updateCars();
        updateHumans();
    }

    public void attach(WindChangeEvent event) {
        super.attach(event);
        this.windChangeEvent = event;
    }

    public void attach(CircuitBreakerEvent event) {
        super.attach(event);
        this.circuitBreakerEvent = event;
    }

    @Override
    public Iterator getHumanIterator() {
        return new HumanIterator(humanList);
    }

    @Override
    public Iterator getPetIterator() {
        return new PetIterator(petList);
    }

    @Override
    public Iterator getRoomIterator() {
        return new RoomIterator(floorList);
    }

    @Override
    public Iterator getSportsEquipmentIterator() {
        return new SportsEquipmentIterator(sportsEquipmentList);
    }

    @Override
    public Iterator getElectronicDeviceIterator() {
        return new DeviceIterator(floorList);
    }
}

package smart_home.human;

import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.electronic_device.api.GasBoilerAPI;
import smart_home.utils.DeviceId;

import java.util.List;

public class Father extends Human {

    public Father() {
        super(true);
    }

    public void turnOffGasBoiler() {
        List<ElectronicDeviceAPI> devices = house.getAllDevices();
        for (ElectronicDeviceAPI device : devices) {
            if (device.getId().equals(DeviceId.GAS_BOILER)) {
                ((GasBoilerAPI) device).turnOff();
            }
        }
    }

    @Override
    public String toString() {
        return "Father";
    }
}

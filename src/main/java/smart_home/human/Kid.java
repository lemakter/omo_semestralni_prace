package smart_home.human;

import smart_home.Car;

public class Kid extends Human {

    public Kid(){
        super(false);
    }

    @Override
    public void setUsing(Car car) {
        // children cannot drive ☺
    }

    @Override
    public String toString(){
        return "Kid";
    }
}

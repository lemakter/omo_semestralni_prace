package smart_home.human;

import smart_home.electronic_device.api.ElectronicDeviceAPI;
import smart_home.utils.DeviceId;

import java.util.List;

public class Mother extends Human {

    public Mother() {
        super(true);
    }

    public void goBuyFood() {
        //change status from broken to idle
        List<ElectronicDeviceAPI> devices = house.getAllDevices();
        for (ElectronicDeviceAPI device : devices) {
            if (device.getId().equals(DeviceId.FRIDGE)) {
                device.fix();
            }
        }
    }

    @Override
    public String toString() {
        return "Mother";
    }
}

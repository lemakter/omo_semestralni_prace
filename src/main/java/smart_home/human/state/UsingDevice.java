package smart_home.human.state;

import smart_home.electronic_device.ElectronicDevice;
import smart_home.human.Human;

public class UsingDevice extends HumanStatus {
    public UsingDevice(Human human) {
        super(human);
    }

    @Override
    public void track(Object object) {
        human.getTracker().trackDeviceUsage((ElectronicDevice) object);
    }

    @Override
    public boolean isActive() {
        return true;
    }
}

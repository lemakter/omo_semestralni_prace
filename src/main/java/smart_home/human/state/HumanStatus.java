package smart_home.human.state;

import smart_home.human.Human;

public abstract class HumanStatus {
    protected final Human human;

    protected HumanStatus(Human human) {
        this.human = human;
    }

    public abstract boolean isActive();

    public abstract void track(Object object);
}

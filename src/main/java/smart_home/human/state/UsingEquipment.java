package smart_home.human.state;

import smart_home.human.Human;
import smart_home.sports_equipment.SportsEquipment;

public class UsingEquipment extends HumanStatus {
    public UsingEquipment(Human human) {
        super(human);
    }

    @Override
    public void track(Object object) {
        human.getTracker().trackEquipmentUsage((SportsEquipment) object);
    }

    @Override
    public boolean isActive() {
        return true;
    }
}

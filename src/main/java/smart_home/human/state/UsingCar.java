package smart_home.human.state;


import smart_home.Car;
import smart_home.human.Human;

public class UsingCar extends HumanStatus {
    public UsingCar(Human human) {
        super(human);
    }

    @Override
    public void track(Object object) {
        human.getTracker().trackCarUsage((Car) object);
    }

    @Override
    public boolean isActive() {
        return true;
    }
}

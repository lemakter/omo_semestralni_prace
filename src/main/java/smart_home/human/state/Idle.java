package smart_home.human.state;

import smart_home.human.Human;

public class Idle extends HumanStatus {


    public Idle(Human human) {
        super(human);
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void track(Object object) {
        //inactive state, no activity to be tracked
    }
}

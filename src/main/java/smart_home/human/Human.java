package smart_home.human;

import lombok.Getter;
import lombok.Setter;
import smart_home.Car;
import smart_home.House;
import smart_home.Room;
import smart_home.electronic_device.ElectronicDevice;
import smart_home.human.state.*;
import smart_home.sports_equipment.SportsEquipment;
import smart_home.trackers.ActivityTracker;

public abstract class Human {

    @Setter
    @Getter
    protected Room occupying;
    protected House house = House.getInstance();
    @Setter
    @Getter
    protected ActivityTracker tracker;
    @Getter
    protected HumanStatus status;
    protected Object using;
    @Getter
    protected final boolean isAdult;
    @Getter
    @Setter
    protected boolean shouldRepairDevice = false;
    @Getter
    @Setter
    protected ElectronicDevice deviceToRepair = null;

    protected Human(boolean isAdult) {
        status = new Idle(this);
        this.isAdult = isAdult;
    }


    public void changeRooms(Room room){
        occupying.getHumansCurrentlyInside().remove(this);
        room.getHumansCurrentlyInside().add(this);
        occupying = room;
    }

    public void setUsing(ElectronicDevice device) {
        if (status.isActive()) return;//if human is already doing something, they cannot do anything else
        using = device;
        status = new UsingDevice(this);
    }

    public void setUsing(Car car) {
        if (status.isActive()) return; //if human is already doing something, they cannot do anything else
        using = car;
        status = new UsingCar(this);
    }

    public void setUsing(SportsEquipment equipment) {
        if (status.isActive()) return;//if human is already doing something, they cannot do anything else
        using = equipment;
        status = new UsingEquipment(this);
    }

    public void update() {
        if (using != null) status.track(using);
        using = null;
        status = new Idle(this);
        if (isAdult && shouldRepairDevice) {
            fixDevice();
        }
    }

    public void fixDevice() {
        deviceToRepair.fix();
        deviceToRepair = null;
        shouldRepairDevice = false;
    }

}

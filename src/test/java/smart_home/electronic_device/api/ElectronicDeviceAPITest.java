package smart_home.electronic_device.api;

import org.junit.Before;
import org.junit.Test;
import smart_home.electronic_device.CDPlayer;
import smart_home.electronic_device.ElectronicDevice;
import smart_home.electronic_device.state.Idle;
import smart_home.events.DeviceBrokenEvent;
import smart_home.human.Human;
import smart_home.trackers.DeviceTracker;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.Mockito.*;

public class ElectronicDeviceAPITest {

    private static ElectronicDeviceAPI deviceAPI;
    private static ElectronicDevice device;
    private static final DeviceBrokenEvent event = mock(DeviceBrokenEvent.class);

    @Before
    public void reset() {
        device = new CDPlayer();
        device.setTracker(new DeviceTracker(0, device));
        new DeviceBrokenEvent(device);
        deviceAPI = new ElectronicDeviceAPI(device);
        device.attach(event);
    }

    @Test
    public void testDeviceActiveTimeIsSetToDefTime() {
        deviceAPI.useDevice(null);
        assertEquals(1, device.getTimeActive());
    }

    @Test
    public void testDeviceActiveTimeIsSetToWantedTime() {
        deviceAPI.useDevice(3, null);
        assertEquals(3, device.getTimeActive());
    }

    @Test
    public void testActiveTimeDecreases() {
        deviceAPI.useDevice(3, null);
        deviceAPI.update();
        assertEquals(2, device.getTimeActive());
    }

    @Test
    public void testActiveStatusTurnOnWhenUsing() {
        deviceAPI.useDevice(null);
        assertFalse(device.getStatus().use());
    }

    @Test
    public void testDeviceStatusTurnsBackIntoPassive() {
        deviceAPI.useDevice(null);
        deviceAPI.update();
        deviceAPI.update();
        assertTrue(device.getStatus().use());
    }

    @Test
    public void testActiveTimeSetsWhenDeviceBreaks() {
        deviceAPI.useDevice(null);
        deviceAPI.deviceBreakDown();
        assertEquals(device.getTimeActive(), -1);
    }

    @Test
    public void testFixSetsStatusToIdle() {
        deviceAPI.deviceBreakDown();
        deviceAPI.fix();
        assertInstanceOf(Idle.class, device.getStatus());
    }

    @Test
    public void testUseDeviceTriggersHumanMethod() {
        Human mock = mock(Human.class);
        deviceAPI.useDevice(mock);
        verify(mock, times(1)).setUsing(device);
    }
}

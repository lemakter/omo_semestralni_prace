package smart_home.trackers;

import org.junit.Before;
import org.junit.Test;
import smart_home.Car;
import smart_home.electronic_device.ElectronicDevice;
import smart_home.human.Human;
import smart_home.sports_equipment.SportsEquipment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class ActivityTrackerTest {

    private static ActivityTracker tracker;
    private static final Human human = mock(Human.class);


    @Before
    public void reset() {
        tracker = new ActivityTracker(0, human);
    }

    @Test
    public void testTimeActiveIncrementsWhenUsingEquipment() {
        tracker.trackEquipmentUsage(mock(SportsEquipment.class));
        assertEquals(1, tracker.getTimeActive());
    }

    @Test
    public void testTimeActiveIncrementsWhenUsingDevice() {
        tracker.trackDeviceUsage(mock(ElectronicDevice.class));
        assertEquals(1, tracker.getTimeActive());
    }

    @Test
    public void testTimeActiveIncrementsWhenUsingCar() {
        tracker.trackCarUsage(mock(Car.class));
        assertEquals(1, tracker.getTimeActive());
    }

    @Test
    public void testTimeActiveIncrementsConsecutively() {
        tracker.trackEquipmentUsage(mock(SportsEquipment.class));
        tracker.trackCarUsage(mock(Car.class));
        tracker.trackDeviceUsage(mock(ElectronicDevice.class));
        assertEquals(3, tracker.getTimeActive());
    }

    @Test
    public void testReportLineIsGenerated() {
        String s = tracker.getReportLine();
        assertNotNull(s);
    }

}

House configuration report:
House has 3 floors.
1. floor has 3 rooms.
2. floor has 2 rooms.
3. floor has 2 rooms.
1. room on 1. floor has 2 windows and current devices: CD player with tracker id 2 , computer with tracker id 3 
2. room on 1. floor has 1 windows and current devices: dryer with tracker id 5 , washing machine with tracker id 6 , phone with tracker id 7 
3. room on 1. floor has 1 windows and current devices: fridge with tracker id 9 , microwave with tracker id 10 , gas boiler with tracker id 11 
1. room on 2. floor has 2 windows and current devices: phone with tracker id 14 , TV with tracker id 15 
2. room on 2. floor has 2 windows and current devices: TV with tracker id 18 , computer with tracker id 19 , CD player with tracker id 20 
1. room on 3. floor has 2 windows and current devices: phone with tracker id 23 , phone with tracker id 24 , CD player with tracker id 25 
2. room on 3. floor has 2 windows and current devices: TV with tracker id 28 , computer with tracker id 29 
Residents of the house are:
1 father
1 mother
4 kids
2 cats
1 dog
The house also has: 
1 ski
2 bikes
1 car
